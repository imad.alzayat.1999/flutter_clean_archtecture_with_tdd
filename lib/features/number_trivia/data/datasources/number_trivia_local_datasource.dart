import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/data/models/number_trivia_model.dart';

abstract class NumberTriviaLocalDataSource{
  Future<NumberTriviaModel> getLastNumberTrivia();
  Future<void> cacheNumberTrivia({required NumberTriviaModel numberTriviaModel});
}