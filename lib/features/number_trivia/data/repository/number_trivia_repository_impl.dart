import 'package:dartz/dartz.dart';
import 'package:flutter_clean_architecture_with_tdd/core/error/exceptions.dart';
import 'package:flutter_clean_architecture_with_tdd/core/error/failure.dart';
import 'package:flutter_clean_architecture_with_tdd/core/network/network_info.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/data/datasources/number_trivia_local_datasource.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/data/datasources/number_trivia_remote_datasource.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/entities/number_trivia_entity.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/repository/number_trivia_repository.dart';

import '../models/number_trivia_model.dart';

typedef Future<NumberTriviaModel> _getConcreteOrRandom();
class NumberTriviaRepositoryImpl extends NumberTriviaRepository {
  final NumberTriviaRemoteDataSource numberTriviaRemoteDataSource;
  final NumberTriviaLocalDataSource numberTriviaLocalDataSource;
  final NetworkInfo networkInfo;

  NumberTriviaRepositoryImpl({
    required this.numberTriviaRemoteDataSource,
    required this.numberTriviaLocalDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, NumberTriviaEntity>> getConcreteNumberTrivia(int? number) async{
    return _getTrivia(() => numberTriviaRemoteDataSource.getConcreteNumberTrivia(number));
  }

  @override
  Future<Either<Failure, NumberTriviaEntity>> getRandomNumberTrivia() async{
    return _getTrivia(() => numberTriviaRemoteDataSource.getRandomNumberTrivia());
  }

  Future<Either<Failure , NumberTriviaEntity>> _getTrivia(_getConcreteOrRandom getConcreteOrRandom) async{
    if(await networkInfo.isConnected){
      try{
        final result = await getConcreteOrRandom();
        numberTriviaLocalDataSource.cacheNumberTrivia(numberTriviaModel: result);
        return Right(result);
      }on ServerException{
        return Left(ServerFailure());
      }
    }else{
      try{
        final localTrivia = await numberTriviaLocalDataSource.getLastNumberTrivia();
        return Right(localTrivia);
      }on CacheException{
        return Left(DatabaseFailure());
      }
    }
  }
}
