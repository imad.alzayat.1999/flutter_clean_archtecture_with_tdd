import 'package:dartz/dartz.dart';
import 'package:flutter_clean_architecture_with_tdd/core/error/failure.dart';
import 'package:flutter_clean_architecture_with_tdd/core/usecase/base_usecase.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/entities/number_trivia_entity.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/repository/number_trivia_repository.dart';

class GetRandomNumberTriviaUseCase extends BaseUseCase<NumberTriviaEntity , NoParameters>{
  final NumberTriviaRepository numberTriviaRepository;

  GetRandomNumberTriviaUseCase({required this.numberTriviaRepository});
  @override
  Future<Either<Failure, NumberTriviaEntity>> call(NoParameters parameters) async{
    return await numberTriviaRepository.getRandomNumberTrivia();
  }
}