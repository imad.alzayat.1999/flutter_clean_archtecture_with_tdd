import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_clean_architecture_with_tdd/core/error/failure.dart';
import 'package:flutter_clean_architecture_with_tdd/core/usecase/base_usecase.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/entities/number_trivia_entity.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/repository/number_trivia_repository.dart';

class GetConcreteNumberTriviaUseCase extends BaseUseCase<NumberTriviaEntity , ConcreteNumberTriviaParameters>{
  final NumberTriviaRepository numberTriviaRepository;

  GetConcreteNumberTriviaUseCase({required this.numberTriviaRepository});

  @override
  Future<Either<Failure, NumberTriviaEntity>> call(ConcreteNumberTriviaParameters parameters) async{
    return await numberTriviaRepository.getConcreteNumberTrivia(parameters.number);
  }
}

class ConcreteNumberTriviaParameters extends Equatable{
  final int number;

  const ConcreteNumberTriviaParameters({required this.number});

  @override
  // TODO: implement props
  List<Object?> get props => [number];
}