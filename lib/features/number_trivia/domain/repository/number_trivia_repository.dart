import 'package:dartz/dartz.dart';
import 'package:flutter_clean_architecture_with_tdd/core/error/failure.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/entities/number_trivia_entity.dart';

abstract class NumberTriviaRepository{
  Future<Either<Failure , NumberTriviaEntity>> getConcreteNumberTrivia(int? number);
  Future<Either<Failure , NumberTriviaEntity>> getRandomNumberTrivia();
}