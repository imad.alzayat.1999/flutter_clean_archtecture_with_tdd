import 'dart:convert';

import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/data/models/number_trivia_model.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/entities/number_trivia_entity.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../fixtures/fixture_reader.dart';

void main(){
  final tNumberTriviaModel = NumberTriviaModel(text: "test", number: 1);

  test("number trivia model", ()  async{
    expect(tNumberTriviaModel, isA<NumberTriviaEntity>());
  });

  group("from json parser", () {
    test("should return a valid model when the number is an integer", ()  async{
      Map<String , dynamic> jsonMap = json.decode(fixture("trivia.json"));
      final result = NumberTriviaModel.fromJson(jsonMap);
      expect(result, tNumberTriviaModel);
    });
    test("should return a valid model when the number is regarded as double", ()  async{
      Map<String , dynamic> jsonMap = json.decode(fixture("trivia_double.json"));
      final result = NumberTriviaModel.fromJson(jsonMap);
      expect(result, tNumberTriviaModel);
    });
    test("should return a JSON map containing the proper data", ()  async{
      final result = tNumberTriviaModel.toJson();
      var properData = {
        "text" : "test text",
        "number": 1,
      };
      expect(result, properData);
    });
  });
}