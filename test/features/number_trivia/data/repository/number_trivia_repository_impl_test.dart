// import 'package:dartz/dartz.dart';
// import 'package:flutter_clean_architecture_with_tdd/core/error/exceptions.dart';
// import 'package:flutter_clean_architecture_with_tdd/core/error/failure.dart';
// import 'package:flutter_clean_architecture_with_tdd/core/network/network_info.dart';
// import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/data/datasources/number_trivia_local_datasource.dart';
// import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/data/datasources/number_trivia_remote_datasource.dart';
// import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/data/models/number_trivia_model.dart';
// import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/data/repository/number_trivia_repository_impl.dart';
// import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/entities/number_trivia_entity.dart';
// import 'package:mockito/annotations.dart';
// import 'package:mockito/mockito.dart';
// import 'package:flutter_test/flutter_test.dart';
//
// class MockNumberTriviaRemoteDataSource extends Mock
//     implements NumberTriviaRemoteDataSource {}
//
// class MockNumberTriviaLocalDataSource extends Mock
//     implements NumberTriviaLocalDataSource {}
//
// class MockNetworkInfo extends Mock implements NetworkInfo {}
//
// @GenerateMocks([
//   MockNumberTriviaRemoteDataSource,
//   MockNumberTriviaLocalDataSource,
//   MockNetworkInfo
// ])
// Future<void> main() async{
//   MockNumberTriviaRemoteDataSource? mockNumberTriviaRemoteDataSource;
//   MockNumberTriviaLocalDataSource? mockNumberTriviaLocalDataSource;
//   MockNetworkInfo? mockNetworkInfo;
//   NumberTriviaRepositoryImpl? numberTriviaRepositoryImpl;
//
//   setUpAll((){
//     mockNumberTriviaLocalDataSource = MockNumberTriviaLocalDataSource();
//     mockNumberTriviaRemoteDataSource = MockNumberTriviaRemoteDataSource();
//     mockNetworkInfo = MockNetworkInfo();
//     numberTriviaRepositoryImpl = NumberTriviaRepositoryImpl(
//       numberTriviaRemoteDataSource: mockNumberTriviaRemoteDataSource!,
//       numberTriviaLocalDataSource: mockNumberTriviaLocalDataSource!,
//       networkInfo: mockNetworkInfo!,
//     );
//   });
//   group("get_concrete_number_trivia", () {
//     final testNumber = 1;
//     final testNumberTriviaModel =
//         NumberTriviaModel(text: "test", number: testNumber);
//     NumberTriviaEntity testNumberTrivia = testNumberTriviaModel;
//
//     test("should check if device is online", () {
//       when(mockNetworkInfo!.isConnected).thenAnswer((_) async => true);
//       mockNumberTriviaRemoteDataSource!.getConcreteNumberTrivia(testNumber);
//       verify(mockNetworkInfo!.isConnected);
//     });
//
//     group("get_concrete_number_trivia", () {
//       final testNumberTriviaModel =
//       NumberTriviaModel(text: "test", number: 123);
//       NumberTriviaEntity testNumberTrivia = testNumberTriviaModel;
//
//       test("should check if device is online", () async {
//         when(mockNetworkInfo!.isConnected).thenAnswer((_) async => true);
//         mockNumberTriviaRemoteDataSource!.getRandomNumberTrivia();
//         verify(mockNetworkInfo!.isConnected);
//       });
//
//       group("test_online" , (){
//         setUp((){
//           when(mockNetworkInfo!.isConnected).thenAnswer((_) async => true);
//         });
//         test("should_implement_remote_data_source_when_calling_is_true",
//                 () async {
//               when(mockNumberTriviaRemoteDataSource!.getRandomNumberTrivia())
//                   .thenAnswer((_) async => testNumberTriviaModel);
//               final result = await numberTriviaRepositoryImpl!
//                   .getRandomNumberTrivia();
//               verify(mockNumberTriviaRemoteDataSource!
//                   .getRandomNumberTrivia());
//               expect(result, equals(Right(testNumberTrivia)));
//             });
//
//         test("should_implement_local_data_source_when_calling_is_true", () async {
//           when(mockNumberTriviaRemoteDataSource!.getRandomNumberTrivia())
//               .thenAnswer((_) async => testNumberTriviaModel);
//           await numberTriviaRepositoryImpl!.getRandomNumberTrivia();
//           verify(mockNumberTriviaRemoteDataSource!
//               .getRandomNumberTrivia());
//           verify(
//             mockNumberTriviaLocalDataSource!.cacheNumberTrivia(
//               numberTriviaModel: testNumberTriviaModel,
//             ),
//           );
//         });
//
//         test("implement_server_failure", () async {
//           when(mockNumberTriviaRemoteDataSource!.getRandomNumberTrivia())
//               .thenThrow(ServerException());
//           final result = await numberTriviaRepositoryImpl!
//               .getRandomNumberTrivia();
//           verify(mockNumberTriviaRemoteDataSource!
//               .getRandomNumberTrivia());
//           verifyZeroInteractions(mockNumberTriviaLocalDataSource);
//           expect(result, equals(Left(testNumberTrivia)));
//         });
//       });
//
//       group("test_offline" , (){
//         setUp((){
//           when(mockNetworkInfo!.isConnected).thenAnswer((_) async => false);
//         });
//         test("should_return_locally_data", () async {
//           when(mockNumberTriviaLocalDataSource!.getLastNumberTrivia())
//               .thenAnswer((_) async => testNumberTriviaModel);
//           final result = await numberTriviaRepositoryImpl!.getConcreteNumberTrivia(testNumber);
//           verifyZeroInteractions(mockNumberTriviaRemoteDataSource);
//           verify(mockNumberTriviaLocalDataSource!.getLastNumberTrivia());
//           expect(result, equals(Right(testNumberTrivia)));
//         });
//
//         test("should_return_cache_failure_when_cache_is_empty", () async {
//           when(mockNumberTriviaLocalDataSource!.getLastNumberTrivia())
//               .thenThrow(CacheException());
//           final result = await numberTriviaRepositoryImpl!.getConcreteNumberTrivia(testNumber);
//           verifyZeroInteractions(mockNumberTriviaRemoteDataSource);
//           verify(mockNumberTriviaLocalDataSource!.getLastNumberTrivia());
//           expect(result, equals(Left(DatabaseFailure())));
//         });
//       });
//     });
//   });
// }
