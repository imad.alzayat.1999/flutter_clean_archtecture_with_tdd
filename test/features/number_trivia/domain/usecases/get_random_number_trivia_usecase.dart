import 'package:dartz/dartz.dart';
import 'package:flutter_clean_architecture_with_tdd/core/usecase/base_usecase.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/entities/number_trivia_entity.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/repository/number_trivia_repository.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/usecases/get_concrete_number_trivia_usecase.dart';
import 'package:flutter_clean_architecture_with_tdd/features/number_trivia/domain/usecases/get_random_number_trivia_usecase.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockNumberTriviaRepository extends Mock
    implements NumberTriviaRepository {}

void main() {
  GetRandomNumberTriviaUseCase? useCase;
  MockNumberTriviaRepository? mockNumberTriviaRepository;

  setUp(() {
    mockNumberTriviaRepository = MockNumberTriviaRepository();
    useCase = GetRandomNumberTriviaUseCase(
        numberTriviaRepository: mockNumberTriviaRepository!);
  });

  final testNumber = 1;
  final testNumberTrivia = NumberTriviaEntity(text: "text", number: testNumber);

  test("should get random number trivia for the number from the repository", () async {
    when(() => mockNumberTriviaRepository!.getRandomNumberTrivia())
        .thenAnswer((_) async => Right(testNumberTrivia));
    final result = await useCase!(const NoParameters());
    expect(result, Right(testNumberTrivia));
    verify(() => mockNumberTriviaRepository!.getRandomNumberTrivia());
  });
}
