import 'package:flutter_clean_architecture_with_tdd/core/network/network_info.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:mocktail/mocktail.dart';

class MockInternetConnectionChecker extends Mock implements InternetConnectionChecker{

}

void main(){
  NetworkInfoImpl? networkInfoImpl;
  MockInternetConnectionChecker? mockInternetConnectionChecker;

  setUp((){
    mockInternetConnectionChecker = MockInternetConnectionChecker();
    networkInfoImpl = NetworkInfoImpl(internetConnectionChecker: mockInternetConnectionChecker!);
  });

  group("is_connected", () {
    test("check_connection", (){
      final testHasConnection = Future.value(true);
      when( () => mockInternetConnectionChecker!.hasConnection).thenAnswer((_) => testHasConnection);
      Future<bool> result = networkInfoImpl!.isConnected;
      reset(mockInternetConnectionChecker);
      verifyNever(() =>  mockInternetConnectionChecker!.hasConnection);
      expect(result, testHasConnection);
    });
  });
}